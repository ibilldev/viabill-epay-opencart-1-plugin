<div class="content" style="text-align: center;">
		<span style="height:49px;width:100%;float:left;" id="">
		<img src="<?php echo $url;?>catalog/view/viabillepay/viabill_logo.png" alt="ViaBill Solution">&nbsp;ViaBill køb nu - betal, når du vil 	</span>
</div>
<div class="buttons">
  <div class="right"><a id="button-confirm" class="button"><span><?php echo $button_confirm; ?></span></a></div>
</div>

<script type="text/javascript" src="https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/paymentwindow.js" charset="UTF-8">
</script>
 
<script type="text/javascript">
	function newWindow() {
        paymentwindow = new PaymentWindow({
			'merchantnumber': "<?php echo $merchantnumber ?>",
			'amount': "<?php echo $amount ?>",
			'currency': "<?php echo $currency ?>",
			'orderid': "<?php echo $orderid ?>",
			'group': "<?php echo $group ?>",
			'smsreceipt': "<?php echo $smsreceipt ?>",
			'mailreceipt': "<?php echo $mailreceipt ?>",
			'windowstate': "<?php echo $windowstate ?>",
			'paymenttype': "<?php echo $paymenttype ?>",
			'paymentcollection': "<?php echo $paymentcollection ?>",
			'lockpaymentcollection' : "<?php echo $lockpaymentcollection ?>",
			'instantcapture': "<?php echo $instantcapture ?>",
			'ownreceipt': "<?php echo $ownreceipt ?>",
			'accepturl': "<?php echo $accepturl ?>",
			'callbackurl': "<?php echo $callbackurl ?>",
			'cancelurl': "<?php echo $cancelurl ?>",
			'description': "<?php echo $description ?>",
			'hash': "<?php echo $hash ?>"
        });
	}
</script>
 
<script type="text/javascript"><!--

var openwindowtries = 0;

$('#button-confirm').bind('click', function() {
	newWindow(); paymentwindow.open(); openwindowtries = 5;
});

function openwindow()
{
	if(openwindowtries < 5)
	{
		if(PaymentWindow != undefined)
		{
			newWindow(); paymentwindow.open(); openwindowtries = 5;
		}
		
		openwindowtries++;
	}
}

$(document).ready(function() {
	setTimeout("openwindow()", 500)	
});

//--></script> 


