<?php 
class ModelPaymentviabillepay extends Model {
    public function getMethod($address) {
    $this->load->language('payment/viabillepay');
    
    if ($this->config->get('viabillepay_status')) {
          $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('vb_epay_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
      
      if (!$this->config->get('vb_epay_geo_zone_id')) {
            $status = TRUE;
          } elseif ($query->num_rows) {
              $status = TRUE;
          } else {
            $status = FALSE;
      } 
        } else {
      $status = FALSE;
    }
    
    $method_data = array();
  
    if ($status) {  
          $method_data = array( 
            'code'         => 'viabillepay',
            'title'      => $this->config->get('vb_epay_payment_name'),
            'sort_order' => $this->config->get('vb_epay_sort_order'),
            'pricetagsrc' => $this->config->get('vb_epay_pricetagsrc')
          );
      }





      // Calculate Totals
    $total_data = array();          
    $total = 0;
    $taxes = $this->cart->getTaxes();
    
    $this->load->model('setting/extension');
    
    $sort_order = array(); 
    
    $results = $this->model_setting_extension->getExtensions('total');
    
    foreach ($results as $key => $value) {
      $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
    }
    
    array_multisort($sort_order, SORT_ASC, $results);
    
    foreach ($results as $result) {
      if ($this->config->get($result['code'] . '_status')) {
        $this->load->model('total/' . $result['code']);
  
        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
      }
    }
  
    $this->load->model('checkout/order');

    $totalsmax = count($total_data)-1;
    $amount = $this->currency->format($total_data[$totalsmax]['value'], $this->session->data['currency'], FALSE, FALSE);

    $method_data['priceTotal'] = $amount;
   
      return $method_data;
    }
}
?>