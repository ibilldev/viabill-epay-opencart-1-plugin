<?php
// Heading
$_['heading_title']      = 'ViaBill/ePay';

// Text 
$_['text_merchantnumber'] = 'Merchant number:';
$_['text_paymentwindow'] = 'Payment window:';
$_['text_viabillepay']        = '<a onclick="window.open(\'http://www.viabill.com\');"><img src="../catalog/view/viabillepay/viabill_logo.png" alt="ViaBill/ePay Payment Solutions" title="ViaBill/ePay Payment Solutions" /></a>';
$_['text_paymentwindow_overlay'] = 'Overlay';
$_['text_paymentwindow_fullscreen'] = 'Full screen';

$_['text_success'] = 'Success: You have modified the ViaBill/ePay details.';

$_['text_ownreceipt'] = 'Own receipt';

$_['text_payment'] = 'Payment name:';

$_['text_help'] = 'Help';

$_['text_logos'] = 'Logos';


$_['text_fee'] = 'Customer pays transaction fee:';

$_['text_service_not_free'] = 'Notice! This service is not free!';
$_['text_group'] = 'Group:';

$_['text_yes'] = 'Yes';
$_['text_no'] = 'No';

$_['md5_disabled'] = 'Deactivated';
$_['md5_a'] = 'Data from ePay';
$_['md5_b'] = 'Data to and from ePay (recommended)';


$_['text_paymentmethods'] = 'Available payment methods:';

// Entry
$_['entry_order_status'] = 'Order status:';
$_['entry_geo_zone']     = 'Geo zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort order:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify ViaBill/ePay!';
?>